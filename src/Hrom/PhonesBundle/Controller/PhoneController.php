<?php

namespace Hrom\PhonesBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Hrom\PhonesBundle\Entity\Phone;
use Hrom\PhonesBundle\Form\PhoneType;
use Symfony\Component\HttpFoundation\Request;

/**
 * Phone controller.
 */
class PhoneController extends Controller
{
    public function newAction($user_id)
    {
        $user = $this->getSingleUser($user_id);

        $phone = new Phone();
        $phone->setUser($user);
        $form = $this->createForm(PhoneType::class, $phone);

        return $this->render('HromPhonesBundle:Phone:form.html.twig', array(
            'phone' => $phone,
            'form' => $form->createView()
        ));
    }

    public function addAction(Request $request, $user_id)
    {
        $user = $this->getSingleUser($user_id);

        $phone = new Phone();
        $phone->setUser($user);
        $form = $this->createForm(PhoneType::class, $phone);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()
                ->getManager();
            $em->persist($phone);
            $em->flush();

            return $this->redirect($this->generateUrl('HromPhonesBundle_user_show', array(
                'id' => $phone->getUser()->getId()))
            );
        }

        return $this->render('HromPhonesBundle:Phone:create.html.twig', array(
            'phone' => $phone,
            'form' => $form->createView()
        ));
    }

    public function getSingleUser($user_id)
    {
        $em = $this->getDoctrine()
            ->getManager();

        $user = $em->getRepository('HromPhonesBundle:User')->find($user_id);

        if (!$user) {
            throw $this->createNotFoundException('Unable to find a contact.');
        }

        return $user;
    }

}