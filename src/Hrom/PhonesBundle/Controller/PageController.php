<?php

namespace Hrom\PhonesBundle\Controller;

use Hrom\PhonesBundle\Form\FormType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Hrom\PhonesBundle\Form\UserType;

class PageController extends Controller
{
    public function indexAction()
    {
        $em = $this->getDoctrine()
            ->getManager();

        $users = $em->getRepository('HromPhonesBundle:User')
            ->getLatestUsers();

        return $this->render('HromPhonesBundle:Page:index.html.twig', array(
            'users' => $users
        ));
    }

    public function addAction()
    {
        return $this->render('HromPhonesBundle:Page:add.html.twig');
    }
}
