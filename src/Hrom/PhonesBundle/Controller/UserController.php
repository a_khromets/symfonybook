<?php

namespace Hrom\PhonesBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class UserController extends Controller
{
    /**
     * Show a contact
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $user = $em->getRepository('HromPhonesBundle:User')->find($id);

        if (!$user) {
            throw $this->createNotFoundException('Unable to find a contact.');
        }

        $phones = $em->getRepository('HromPhonesBundle:Phone')
            ->getPhonesForUser($user->getId());

        return $this->render('HromPhonesBundle:User:show.html.twig', array(
            'user' => $user,
            'phones' => $phones
        ));
    }
}
