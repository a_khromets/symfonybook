<?php

namespace Hrom\PhonesBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Hrom\PhonesBundle\Entity\Phone;
use Hrom\PhonesBundle\Entity\User;

class PhoneFixtures extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $phone = new Phone();
        $phone->setPhone('23947234');
        $phone->setUser($manager->merge($this->getReference('user-1')));
        $manager->persist($phone);

        $phone = new Phone();
        $phone->setPhone('32482394');
        $phone->setUser($manager->merge($this->getReference('user-2')));
        $manager->persist($phone);

        $phone = new Phone();
        $phone->setPhone('029834');
        $phone->setUser($manager->merge($this->getReference('user-3')));
        $manager->persist($phone);

        $phone = new Phone();
        $phone->setPhone('21123');
        $phone->setUser($manager->merge($this->getReference('user-4')));
        $manager->persist($phone);

        $phone = new Phone();
        $phone->setPhone('324234982');
        $phone->setUser($manager->merge($this->getReference('user-5')));
        $manager->persist($phone);

        $manager->flush();
    }

    public function getOrder()
    {
        return 2;
    }
}