<?php

namespace Hrom\PhonesBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Hrom\PhonesBundle\Entity\User;

class UserFixtures extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $user1 = new User();
        $user1->setName('John');
        $user1->setSurname('Smith');
        $manager->persist($user1);

        $user2 = new User();
        $user2->setName('Morgan');
        $user2->setSurname('Freeman');
        $manager->persist($user2);

        $user3 = new User();
        $user3->setName('Jason');
        $user3->setSurname('Statham');
        $manager->persist($user3);

        $user4 = new User();
        $user4->setName('Jimmy');
        $user4->setSurname('Hendrix');
        $manager->persist($user4);

        $user5 = new User();
        $user5->setName('Sara');
        $user5->setSurname('Connor');
        $manager->persist($user5);

        $manager->flush();

        $this->addReference('user-1', $user1);
        $this->addReference('user-2', $user2);
        $this->addReference('user-3', $user3);
        $this->addReference('user-4', $user4);
        $this->addReference('user-5', $user5);
    }

    public function getOrder()
    {
        return 1;
    }

}